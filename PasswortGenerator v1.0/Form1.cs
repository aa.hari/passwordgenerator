﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows;


namespace PasswortGenerator_v1._0
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            txtInput.Select();
        }

        private int[] passwordAlgorithem(byte[] asciiNumbers, string userInput)
        {
            var editedNumbers = new int[12];
            editedNumbers[0] = (asciiNumbers[0] * asciiNumbers[3]) % 87 + 35;
            for (var i = 1; i < userInput.Length; i++)
            {
                editedNumbers[i] = (asciiNumbers[i - 1] * asciiNumbers[i]) % 87 + 35;
            }
            
            if (userInput.Length != 12)
            {
                for (var i = userInput.Length; i < (userInput.Length * 2) && i < 12; i++)
                {
                    editedNumbers[i] = Convert.ToInt32(Math.Pow(editedNumbers[i - userInput.Length], 3) % 87 + 35);
                }

                for (var i = userInput.Length * 2; i < 12; i++)
                {
                    editedNumbers[i] = Convert.ToInt32(Math.Pow(editedNumbers[i - userInput.Length], 3) % 87 + 35);
                }
            }
            
            editedNumbers = CheckForDouplicates(editedNumbers);
            return CheckIfValidSign(editedNumbers);
        }

        private int[] CheckForDouplicates(int[] editedNumbers)
        {
            for (var i = 1; i < 12; i++)
            {
                for (var j = 1; j < 12; j++)
                {
                    if (editedNumbers[i] == editedNumbers[j] && i != j)
                    {
                        editedNumbers[i] = Convert.ToInt32((Math.Pow(editedNumbers[i], 5) + i * -j) % 87 + 35);
                    }
                }
            }
            return editedNumbers;
        }
        private int[] CheckIfValidSign(int[] editedNumbers)
        {
            for (var i = 0; i < 12; i++)
            {
                switch (editedNumbers[i])
                {
                    case 36:
                        --editedNumbers[i];
                        break;
                    case 38:
                        --editedNumbers[i];
                        break;
                    case 39:
                        ++editedNumbers[i];
                        break;
                    case 44:
                        ++editedNumbers[i];
                        break;
                    case 58:
                        --editedNumbers[i];
                        break;
                    case 59:
                        ++editedNumbers[i];
                        break;
                    case 62:
                        --editedNumbers[i];
                        break;
                    case 92:
                        --editedNumbers[i];
                        break;
                    case 94:
                        --editedNumbers[i];
                        break;
                    case 96:
                        --editedNumbers[i];
                        break;
                }
            }
            return editedNumbers;
        }

        public void screenPassword(int[] editedNumbers)
        {
            var byteOutput = new byte[12];
            for (var i = 0; i < 12; i++)
            {
                byteOutput[i] = Convert.ToByte(editedNumbers[i]);
            }
            var passwordOutput = Encoding.ASCII.GetString(byteOutput);
            lblOutput.Text = passwordOutput;
            Clipboard.SetText(passwordOutput);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            var userInput = txtInput.Text;
            if (userInput.Length >= 4)
            {
                var asciiNumbers = Encoding.ASCII.GetBytes(userInput);
                var editedNumbers = passwordAlgorithem(asciiNumbers, userInput);
                screenPassword(editedNumbers);
            }
            else
            {
                txtInput.Text = "";
                lblOutput.Text = "";
            }
            
        }
        
        private void txtInput_TextChanged(object sender, EventArgs e)
        {
            if (!txtInput.Text.All(chr => char.IsLetter(chr)))
            {
                txtInput.Text = "";
            }
        }
        
        private void txtInput_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                button1.PerformClick();
            }
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            Clipboard.Clear();
        }
    }
}